/*
 * nco.h
 */

#ifndef INC_NCO_H_
#define INC_NCO_H_

void nco_update_accumulator(void);
void nco_set_output_frequency(float frequency);

#endif /* INC_NCO_H_ */
