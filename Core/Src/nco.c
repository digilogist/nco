/*
 * nco.c
 */

#include <stdint.h>
#include "stm32g4xx_hal.h"
#include "nco.h"
#include "main.h"

static uint32_t accumulator;
static uint32_t step_size;

#define PWM_TIMER_FREQUENCY 			    CPU_FREQUENCY
#define ACCUMULATOR_TIMER_FREQUENCY 		CPU_FREQUENCY
#define ACCUMULATOR_TIMER_CYCLE_PERIODE 	250U /* ACCUMULATOR_TIMER_ARR + 1 */
#define ACCUMULATOR_FREQUENCY 			    ((float) ACCUMULATOR_TIMER_FREQUENCY / ACCUMULATOR_TIMER_CYCLE_PERIODE)

void nco_update_accumulator(void)
{

#if 0
	if (accumulator < step_size) {
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET);
	} else if (accumulator >= (UINT32_MAX / 2)) {
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);
	}

#else

	if (accumulator < step_size) {
		/* Disable Counter */
		TIM2->CR1 &= ~(TIM_CR1_CEN);
		/* Generate Update Event to force update CCR1, CCR2 and ARR register.*/
		TIM3->EGR |= TIM_EGR_UG;
		/* Calculate interpolated counter value for TIMs for the next upcoming NCO trigger */
		uint32_t counter_reset_value = ((float) accumulator * ACCUMULATOR_TIMER_CYCLE_PERIODE) / step_size;
		/* Synchronize drive signal generation timer counter value for frequency 1 */
		TIM2->CNT = counter_reset_value;
		/* Activate hardware trigger */
		TIM2->SMCR |= 0b110;
	} else {
		/* Deactivate synchronization of timers */
		TIM2->SMCR &= ~(0b110);
	}
#endif
	accumulator += step_size;
}

void nco_set_output_frequency(float frequency)
{
	step_size = (frequency / ACCUMULATOR_FREQUENCY) * UINT32_MAX;

	float timer2_periode = (float) PWM_TIMER_FREQUENCY / frequency;
	/* Position of rising edge */
	TIM2->CCR1 = (uint32_t) (timer2_periode * 0.25F);
	/* Position of falling edge */
	TIM2->CCR2 = (uint32_t) (timer2_periode * 0.75F);
	/* Period time for this frequency */
	TIM2->ARR = (uint32_t) timer2_periode;
}
